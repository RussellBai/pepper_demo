/* 
 * File:   main.c
 * Author: user
 *
 * Created on 2017年6月1日, 上午 10:15
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <strings.h>
#include <stdio.h>

#include "main.h"
/*
 * 
 */
int main()
{
	int	inRetVal;
	int	inSockfd = -1;
	struct	sockaddr_in stDest;
	char	szBuffer[128];
	char	szSendMsg[12 + 1] = "";
	char	szAddress[16 + 1] = "";
	
	/* Read IP */
	memset(szAddress, 0x00, sizeof(szAddress));
	inRetVal = inGetIP(szAddress);
	if (inRetVal != EXIT_SUCCESS)
	{
		printf("read IP fail.");
		return (-1);
	}
	else
	{
		printf("IP : %s\n", szAddress);
	}
	
	/* Read Data */
	memset(szSendMsg, 0x00, sizeof(szSendMsg));
	inRetVal = inGetAmount(szSendMsg);
	if (inRetVal != EXIT_SUCCESS)
	{
		printf("Read Data fail.");
		return (-1);
	}
	else
	{
		printf("Data : %s\n", szSendMsg);
	}
	
	/* Create socket */
	inSockfd = socket(PF_INET, SOCK_STREAM, 0);

	/* Initialize value in dest */
	memset(&stDest, 0x00, sizeof(stDest));
	stDest.sin_family = PF_INET;
	stDest.sin_port = htons(9740);
	inet_aton(szAddress, &stDest.sin_addr);

	/* Connecting to server */
	connect(inSockfd, (struct sockaddr*)&stDest, sizeof(stDest));
	if (inSockfd > 0)
	{
		printf("Connet OK\n");
	}
	else
	{
		printf("Connet Fail\n");
	}
	
	/* Send Data */
	inRetVal = send(inSockfd, szSendMsg, strlen(szSendMsg), 0);
	if (inRetVal >= 0)
	{
		printf("Send Len:%d\n", inRetVal);
		printf("%s\n", szSendMsg);
	}
	else
	{
		printf("Send Fail\n");
	}
	

	/* Receive message from the server and print to screen */
	memset(szBuffer, 0x00, sizeof(szBuffer));
	inRetVal = recv(inSockfd, szBuffer, sizeof(szBuffer), 0);
	if (inRetVal >= 0)
	{
		printf("Receive Len:%d\n", inRetVal);
		printf("%s\n", szBuffer);
	}
	else
	{
		printf("Receive Fail\n");
	}

	/* Close connection */
	close(inSockfd);
	
	/* pause */
	fgetc(stdin);
	
	/* create socket */
	return (EXIT_SUCCESS);
}

/*
Function        :inGetIP
Date&Time       :2017/6/4 下午 2:49
Describe        :
 */
int inGetIP(char *szIP)
{
	char	szFileName[20 + 1] = "config.txt";
	FILE	*fPtr;
	
	fPtr = fopen(szFileName, "rb");
	if (fPtr)
	{
		printf("open sucess...\n");
	}
	else
	{
		printf("open fail...\n");
		
		return (-1);
	}
	
	fscanf(fPtr, "%s", szIP);
	
	fclose(fPtr);
	
	return (EXIT_SUCCESS);
}

/*
Function        :inGetAmount
Date&Time       :2017/6/5 上午 3:49
Describe        :
 */
int inGetAmount(char *szAmt)
{
	char	szFileName[20 + 1] = "amount.txt";
	FILE	*fPtr;
	
	fPtr = fopen(szFileName, "rb");
	if (fPtr)
	{
		printf("open sucess...\n");
	}
	else
	{
		printf("open fail...\n");
		
		return (-1);
	}
	
	fscanf(fPtr, "%s", szAmt);
	
	fclose(fPtr);
	
	return (EXIT_SUCCESS);
}
